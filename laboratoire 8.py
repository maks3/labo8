import numpy as np
import matplotlib.pyplot as plt



def methodeEuler(f0,h,fct):
    t=np.arange(-1.,1.,h)
    f=np.empty_like(t)
    f[0]=f0
    for i in range(0,t.shape[0]-1): f[i+1]=f[i]+h*(fct(t[i],f[i]))
    return f

def methodeEuler2(f0,h,fct):
    t=np.arange(-1.,1.,h)
    f=np.empty_like(t)
    f[0]=f0
    for i in range(0,t.shape[0]-1): f[i+1]=f[i]+h*(fct[i])
    return f

def dudt(x,y):
    return -((G*M)/((x**2+y**2)**(3/2)))*x

def dvdt(x,y):
    return -((G*M)/((x**2+y**2)**(3/2)))*y

G=4*np.pi**2
M=1
R0=1

u0=(G*M/R0)
u=methodeEuler(u0,.01,dudt)

v0=0
v=methodeEuler(v0,.01,dvdt)

x0=0
x=methodeEuler2(x0,.01,u)

y0=-R0
y=methodeEuler2(y0,.01,v)

plt.plot(y)
plt.show()